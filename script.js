//VARIÁVEIS GLOBAIS

//div de saída do resultado
let divOutput = document.getElementById('saida')
//elements para atribuir os resultados
let jogada = document.createElement('p')
let noOptionSelectedMessage = document.createElement('p')
let messageWinner = document.createElement('div')

//salvar a opção selecionada com onClick
let optionSelected = ""
function setOptionSelected(option) {

    optionSelected = option
}

//limpar o resultado para a próxima jogada
let limpandoResultado = ""

//atribuindo códigos em Pedra, Papel e Tesoura
let pedra = 0
let papel = 1
let tesoura = 2

//variáveis para atribuir os emojis conforme a escolha
let emojiPlayer = ''
let emojiCPU = ''

//Atribuindo os Emojis nas Variáveis usando Unicode src: https://unicode.org/emoji/charts/full-emoji-list.html
let emojiPedra = `\u{1F44A}`
let emojiPapel = `\u{1F91A}`
let emojiTesoura = `\u{270C}`

//Atribundo os Emojis nas li's
let optionPedra = document.getElementById('0')
optionPedra.innerHTML += emojiPedra
optionPedra.addEventListener("click", (e) => {
    setOptionSelected(e.target.id)
    playGame()
})

let optionPapel = document.getElementById('1')
optionPapel.innerHTML += emojiPapel
optionPapel.addEventListener("click", (e) => {
    setOptionSelected(e.target.id)
    playGame()
})

let optionTesoura = document.getElementById('2')
optionTesoura.innerHTML += emojiTesoura
optionTesoura.addEventListener("click", (e) => {
    setOptionSelected(e.target.id)
    playGame()
})

//Atribuindo a function playGame() em button#btn
// let buttonJogar = document.getElementById('btn')
// buttonJogar.addEventListener('click', playGame)

//Função Principal do Jogo
function playGame () {

    divOutput.innerHTML = limpandoResultado
    messageWinner.innerHTML = limpandoResultado

    noOptionSelected = optionSelected.length < 1 ? true : false

    if (noOptionSelected) {

        noOptionSelectedMessage.innerHTML = "Escolha uma das Opções"
        noOptionSelectedMessage.style.fontSize = "16px"

        divOutput.appendChild(noOptionSelectedMessage)

        return
    }

    //return emojiPlayer
    selectEmojiPlayer()
    //return emojiCPU
    selectEmojiCPU()

    //Resultado
    jogada.innerHTML = `${emojiPlayer}x${emojiCPU}`
    messageWinner.appendChild(checkWinner(emojiPlayer, emojiCPU))

    divOutput.appendChild(jogada)
    divOutput.appendChild(messageWinner)
}

//Function Verificando o Vencedor
function checkWinner (player, cpu) {

    let imageWinner = document.createElement('img')

    //se: player win
    if (player == emojiPapel && cpu == emojiPedra
        || player == emojiTesoura && cpu == emojiPapel 
        || player == emojiPedra && cpu == emojiTesoura) {

        imageWinner.setAttribute('src', 'imgs/win.png')
    //se não se: CPU win
    } else if (cpu == emojiPapel && player == emojiPedra
        || cpu == emojiTesoura && player == emojiPapel
        || cpu == emojiPedra && player == emojiTesoura) {

        imageWinner.setAttribute('src', 'imgs/lose.png')
    } 
    else {
        // se não: empate
        imageWinner.setAttribute('src', 'imgs/friendly.png')
    }

    return imageWinner
}

//function Selecionando o Emoji do Player
function selectEmojiPlayer () {

    if (optionSelected == pedra) {
        emojiPlayer = emojiPedra
    } else if (optionSelected == papel) {
        emojiPlayer = emojiPapel
    }
    else {
        emojiPlayer = emojiTesoura
    }
}

//function Selecionando o Emoji do CPU aleatóriamente
function selectEmojiCPU () {

    let randomChoice = Math.floor(Math.random() * 3)

    if (randomChoice === pedra) {
        emojiCPU = emojiPedra
    } 
    if (randomChoice === papel){
        emojiCPU = emojiPapel
    } 
    if (randomChoice === tesoura) {
        emojiCPU = emojiTesoura
    }
}